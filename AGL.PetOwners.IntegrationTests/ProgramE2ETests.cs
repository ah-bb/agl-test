﻿using System;
using System.IO;
using AGL.PetOwners.Console;
using FluentAssertions;
using NUnit.Framework;

namespace AGL.PetOwners.IntegrationTests
{
    [TestFixture]
    public class ProgramE2ETests
    {
        [Test]
        public void Main_End2EndTest_ReturnsExpectedResults()
        {
            //arrange
            var expectedResults = new[]
            {
                "Male",
                "* Garfield",
                "* Jim",
                "* Max",
                "* Tom",
                "",
                "Female",
                "",
                "* Garfield",
                "* Simba",
                "* Tabby"
            };

            RunAndAssertConsoleLog(expectedResults);
        }

        private static void RunAndAssertConsoleLog(string[] expectedValuesInConsoleLog)
        {
            //act
            string consoleLog;

            using (var sw = new StringWriter())
            {
                System.Console.SetOut(sw);

                Program.Main();

                consoleLog = sw.ToString();
            }

            //assert
            Assert.IsNotEmpty(consoleLog);
            var lines = consoleLog.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            expectedValuesInConsoleLog.Should().BeSubsetOf(lines);
        }
    }
}