﻿using System.Threading.Tasks;
using AGL.PetOwners.Console.Bootstrap;
using AGL.PetOwners.Console.Services;
using FluentAssertions;
using Microsoft.Practices.Unity;
using NUnit.Framework;

namespace AGL.PetOwners.IntegrationTests.Services
{
    [TestFixture]
    public class PetOwnershipAnalysisServiceE2ETests
    {
        private static PetOwnershipAnalysisService CreatePetOwnershipAnalysisService()
        {
            var ioc = IocConfig.RegisterComponents();
            return ioc.Resolve<PetOwnershipAnalysisService>();
        }

        [Test]
        public async Task RunAnalysisAsync_End2EndTest_ReturnsExpectedResults()
        {
            //arrange
            var expectedResults = new[]
            {
                "Male",
                "",
                "* Garfield",
                "* Jim",
                "* Max",
                "* Tom",
                "",
                "Female",
                "",
                "* Garfield",
                "* Simba",
                "* Tabby"
            };

            var sut = CreatePetOwnershipAnalysisService();

            //act
            var results = await sut.RunAnalysisAsync();

            //assert
            Assert.IsNotNull(results);
            results.ShouldAllBeEquivalentTo(expectedResults, options => options.WithStrictOrdering());
        }
    }
}