﻿using System;
using System.Net;
using System.Threading.Tasks;
using AGL.PetOwners.Console.Gateways;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using Serilog;

namespace AGL.PetOwners.IntegrationTests.Gateways
{
    [TestFixture]
    public class HttpDataRetrievalServiceTests
    {
        [Test]
        public async Task FetchContentAsync_GetAglDevTestData_ReturnsString()
        {
            //arrange
            var loggerMock = new Mock<ILogger>();
            var sut = new HttpDataRetrievalService(loggerMock.Object);

            //act
            var results = await sut.FetchContentAsync(new Uri(@"http://agl-developer-test.azurewebsites.net/people.json", UriKind.Absolute));
            
            //assert
            results.Should().NotBeEmpty();
        }

        [TestCase(@"http://agl-developer-test.azurewebsites.net/people.json.not.there", HttpStatusCode.NotFound)]
        [TestCase(@"http://the-internet.herokuapp.com/status_codes/500", HttpStatusCode.InternalServerError)]
        public void FetchContentAsync_WhenGetDoesNotReturn200Ok_ThrowsApplicationException(string url, HttpStatusCode expectedHttpStatusCode)
        {
            //arrange
            var notFoundUri = new Uri(url, UriKind.Absolute);
            var loggerMock = new Mock<ILogger>();
            var sut = new HttpDataRetrievalService(loggerMock.Object);

            //act and assert
            var ex = Assert.ThrowsAsync<ApplicationException>(async () => await sut.FetchContentAsync(notFoundUri));
            Assert.IsNotNull(ex.Message);
            Assert.AreEqual($"Get {notFoundUri}: Expected 200 OK but got {expectedHttpStatusCode.ToString("D")} {expectedHttpStatusCode}", ex.Message);
        }
    }
}
