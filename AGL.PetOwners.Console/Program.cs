﻿using System;
using AGL.PetOwners.Console.Bootstrap;
using AGL.PetOwners.Console.Services;
using Microsoft.Practices.Unity;

namespace AGL.PetOwners.Console
{
    public class Program
    {
        public static void Main()
        {
            try
            {
                var ioc = IocConfig.RegisterComponents();

                // Explicitly Resolve the "root" component
                var petOwnershipAnalysisService = ioc.Resolve<IPetOwnershipAnalysisService>();

                //Bit of odd bootstrapping here as this demo application is running in a console app without native async/await 
                var results = petOwnershipAnalysisService.RunAnalysisAsync().GetAwaiter().GetResult();
                foreach (var line in results)
                {
                    System.Console.WriteLine(line);
                }
            }
            catch (Exception e)
            {
                System.Console.WriteLine($"Error whilst running the PetOwnershipAnalysisService: {e.Message}");
            }

            if (!System.Console.IsInputRedirected)
            {
                System.Console.WriteLine("\npress any key to continue");
                System.Console.ReadKey();
            }
        }
    }
}
