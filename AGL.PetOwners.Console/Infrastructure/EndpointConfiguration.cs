using System;
using System.Configuration;

namespace AGL.PetOwners.Console.Infrastructure
{
    public class EndpointConfiguration : IEndpointConfiguration
    {
        public Uri AglDeveloperTestPetOwnersEndpoint => ReadUriSettingFromConfig("AGLDeveloperTestPetOwnersEndpoint");

        private Uri ReadUriSettingFromConfig(string settingName)
        {
            var endpointUrl = ConfigurationManager.AppSettings[settingName];

            if (string.IsNullOrEmpty(endpointUrl))
            {
                throw new ApplicationException($"Value for {settingName} value not set correctly in app.config");
            }

            return new Uri(endpointUrl, UriKind.Absolute);
        }
    }
}