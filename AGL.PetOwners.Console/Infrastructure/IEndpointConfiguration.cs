﻿using System;

namespace AGL.PetOwners.Console.Infrastructure
{
    public interface IEndpointConfiguration
    {
        Uri AglDeveloperTestPetOwnersEndpoint { get; }
    }
}