﻿using System;
using System.Threading.Tasks;

namespace AGL.PetOwners.Console.Gateways
{
    public interface IDataRetrievalService
    {
        Task<string> FetchContentAsync(Uri uri);
    }
}