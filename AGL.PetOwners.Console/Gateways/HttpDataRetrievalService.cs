﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Serilog;

namespace AGL.PetOwners.Console.Gateways
{
    public class HttpDataRetrievalService : IDataRetrievalService
    {
        private readonly ILogger _logger;

        public HttpDataRetrievalService(ILogger logger)
        {
            _logger = logger;
        }

        public async Task<string> FetchContentAsync(Uri uri)
        {
            _logger.Information($"Fetching content from {uri}");

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = await client.GetAsync(uri);

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    _logger.Error($"Failed to load content from {uri}, returned {response.StatusCode}");
                    throw new ApplicationException($"Get {uri}: Expected 200 OK but got {response.StatusCode.ToString("D")} {response.StatusCode}");
                }

                var content = await response.Content.ReadAsStringAsync();
                return content;
            }
        }
    }
}