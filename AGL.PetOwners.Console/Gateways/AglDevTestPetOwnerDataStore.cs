﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AGL.PetOwners.Console.Infrastructure;
using AGL.PetOwners.Console.Models;
using Newtonsoft.Json;
using Serilog;

namespace AGL.PetOwners.Console.Gateways
{
    public class AglDevTestPetOwnerDataStore : IPetOwnerDataStore
    {
        private readonly ILogger _logger;
        private readonly IDataRetrievalService _dataRetrievalService;
        private readonly IEndpointConfiguration _endpointConfig;

        public AglDevTestPetOwnerDataStore(ILogger logger, IDataRetrievalService dataRetrievalService, IEndpointConfiguration endpointConfig)
        {
            _logger = logger;
            _dataRetrievalService = dataRetrievalService;
            _endpointConfig = endpointConfig;
        }

        public async Task<IList<PetOwner>> FetchPetOwnersAsync()
        {
            var contents = await _dataRetrievalService.FetchContentAsync(_endpointConfig.AglDeveloperTestPetOwnersEndpoint);

            try
            {
                //We could add more specific validation here depending on our knowledge of the source of the data and how we intend to use it
                //Other implementations of the IPetOwnerDataStore.FetchPetOwnersAsync method might have to manipulate the source data before it is 
                //mapped into our PetOwner domain model 

                //Deserializing directly into a List<PetOwner> is brittle but I'm ok with it in a demo application
                var allPeople = JsonConvert.DeserializeObject<List<PetOwner>>(contents);
                var peopleWithPets = allPeople.Where(p => p.Pets?.Count > 0).ToList();

                _logger.Information("Loaded {allCount} people of which {ownerCount} own pets from the dataRetrievalService", 
                                    allPeople.Count, peopleWithPets.Count);

                return peopleWithPets;
            }
            catch (Exception e)
            {
                throw new ApplicationException("Unexpected PetOwner data in json results retrieved from the DataRetrievalService", e);
            }
        }
    }
}
