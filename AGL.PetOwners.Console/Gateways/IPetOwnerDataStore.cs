﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AGL.PetOwners.Console.Models;

namespace AGL.PetOwners.Console.Gateways
{
    public interface IPetOwnerDataStore
    {
        Task<IList<PetOwner>> FetchPetOwnersAsync();
    }
}