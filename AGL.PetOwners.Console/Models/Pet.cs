﻿using System;
using System.Diagnostics;

namespace AGL.PetOwners.Console.Models
{
    [DebuggerDisplay("{Name} {Type}")]
    public class Pet
    {
        public string Name { get; }

        public string Type { get; } //Could be a more specific type than 'string' given more knowledge of the domain

        public bool IsOfType(string type)
        {
            return Type.Equals(type, StringComparison.OrdinalIgnoreCase);
        }

        public Pet(string name, string type)
        {
            Name = name;
            Type = type;
        }
    }
}