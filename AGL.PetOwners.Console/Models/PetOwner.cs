﻿using System.Collections.Generic;

namespace AGL.PetOwners.Console.Models
{
    public class PetOwner
    {
        public string Name { get; }

        public Gender Gender { get; }

        public int Age { get; }

        public IList<Pet> Pets { get; }

        public PetOwner(string name, Gender gender, int age, IList<Pet> pets)
        {
            Name = name;
            Gender = gender;
            Age = age;
            Pets = pets;
        }
    }
}