﻿using System.Collections.Generic;
using System.Linq;
using AGL.PetOwners.Console.Models;

namespace AGL.PetOwners.Console.Filters
{
    public class CatsByOwnerGenderAggregator : IPetAggregator 
    {
        public IDictionary<string, IList<Pet>> Aggregate(IList<PetOwner> petOwners) 
        {
            if (petOwners == null)
            {
                return new Dictionary<string, IList<Pet>>();
            }

            const string petType = "cat";
            var results = petOwners.Where(po => po.Pets != null && po.Pets.Any(p => p.IsOfType(petType)))
                                   .GroupBy(po => po.Gender)
                                   .Select(group => new
                                    {
                                        Gender = group.Key,
                                        Pets = group.SelectMany(v => v.Pets.Where(p => p.IsOfType(petType)))
                                                    //Order by name here rather than implement IComparable on Pet or add an IComparer<Pet>
                                                    .OrderBy(n => n.Name) 
                                                    .ToList()
                                    })
                                   .ToDictionary(res => res.Gender.ToString(), 
                                                 res => (IList<Pet>)res.Pets);
            return results;
        }
    }
}