﻿using System.Collections.Generic;
using AGL.PetOwners.Console.Models;

namespace AGL.PetOwners.Console.Filters
{
    public interface IPetAggregator
    {
        IDictionary<string, IList<Pet>> Aggregate(IList<PetOwner> petOwners);
    }
}
