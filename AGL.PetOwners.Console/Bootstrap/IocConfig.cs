﻿using AGL.PetOwners.Console.Filters;
using AGL.PetOwners.Console.Gateways;
using Microsoft.Practices.Unity;
using Serilog;

namespace AGL.PetOwners.Console.Bootstrap
{
    public class IocConfig
    {
        public static IUnityContainer RegisterComponents()
        {
            IUnityContainer container = new UnityContainer();

            container.RegisterType<ILogger>(new InjectionFactory((cntr, type, name) =>
            {
                //Basic config to write logs to the console
                return new LoggerConfiguration()
                        .WriteTo.Console()
                        .CreateLogger();
            }));

            //Register all types using WithMappings.FromMatchingInterface so ObjectExample is always created when requesting IObjectExample
            container.RegisterTypes(AllClasses.FromAssemblies(typeof(IocConfig).Assembly),
                                    WithMappings.FromMatchingInterface, WithName.Default, WithLifetime.Transient);


            //The following allows for future extensibility, for example we could use different data sources or different aggregator implementations
            //without modifying the existing code (new classes could be written and configured here) 
            container.RegisterType<IDataRetrievalService, HttpDataRetrievalService>();
            container.RegisterType<IPetOwnerDataStore, AglDevTestPetOwnerDataStore>();
            container.RegisterType<IPetAggregator, CatsByOwnerGenderAggregator>();

            /* alternatively we could use an injection factory to decide upon the type of analysis and data sources used:
                       
            container.RegisterType<IPetOwnershipAnalysisService>(new TransientLifetimeManager(),
                                                                 new InjectionFactory(c =>
                                                                 {
                                                                     var dataStore = c.Resolve<AglDevTestPetOwnerDataStore>();
                                                                     var aggregator = c.Resolve<CatsByOwnerGenderAggregator>();

                                                                     return new PetOwnershipAnalysisService(dataStore, aggregator);
                                                                 }));
            */

            return container;
        }
    }
}
