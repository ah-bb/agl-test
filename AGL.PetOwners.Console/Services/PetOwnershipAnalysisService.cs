﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AGL.PetOwners.Console.Filters;
using AGL.PetOwners.Console.Gateways;
using AGL.PetOwners.Console.Models;

namespace AGL.PetOwners.Console.Services
{
    public class PetOwnershipAnalysisService : IPetOwnershipAnalysisService
    {
        private readonly IPetOwnerDataStore _petOwnerDataStore;
        private readonly IPetAggregator _petAggregator;

        public PetOwnershipAnalysisService(IPetOwnerDataStore petOwnerDataStore, IPetAggregator petAggregator)
        {
            _petOwnerDataStore = petOwnerDataStore;
            _petAggregator = petAggregator;
        }

        public async Task<IList<string>> RunAnalysisAsync()
        {
            var petOwners = await _petOwnerDataStore.FetchPetOwnersAsync();

            var petsByGroup = _petAggregator.Aggregate(petOwners);

            return ConvertToOutputFormat(petsByGroup);
        }

        private static IList<string> ConvertToOutputFormat(IDictionary<string, IList<Pet>> petsByGroup)
        {
            var results = new List<string>();
            foreach (var group in petsByGroup)
            {
                if (results.Count > 0)
                {
                    results.Add("");
                }

                results.Add(group.Key);
                results.Add("");
                foreach (var pet in group.Value)
                {
                    results.Add($"* {pet.Name}");
                }
            }
            return results;
        }
    }
}