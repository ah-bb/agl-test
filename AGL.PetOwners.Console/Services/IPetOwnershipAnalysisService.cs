﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace AGL.PetOwners.Console.Services
{
    public interface IPetOwnershipAnalysisService
    {
        Task<IList<string>> RunAnalysisAsync();
    }
}