﻿Hi AGL Dev Team,

Having assumed that this programming exercise is focused on system structure/design and testability, I've built a solution consisting of a .net C# console application and associated test projects built in Visual studio 2017. Using a framework with a more featured UI would probably not add much benefit to my solution. 

Future extensibility in my design centres around allowing new PetOwner data formats/sources (via implementations of the IPetOwnerDataStore interface), retrieved from different sources (via an IDataRetrievalService implementation) and analysed with different queries (via IPetAggregator implementations).

For example: we could implement a ThirdPartyPetOwnerDataStore that uses XML from a FileDataRetrievalService (rather than the current AGL Developer Test json data over http) and pass its results through a new DogsByOwnerAgeAggregator. These would be configured for use via IOC in the IocConfig class without code changes to the existing codebase. 

The 3 assemblies in this solution are: 

#### AGL.PetOwners.Console
- The main program - the entry point is effectively the RunAnalysisAsync method in the PetOwnershipAnalysisService class in the Services folder.
 
#### AGL.PetOwners.IntegrationTests
- Integration tests for the application 'boundaries' (i.e the http service call)
- End-2-End tests

#### AGL.PetOwners.UnitTests
- Unit tests focusing on the internal logic of the application.

Regards
Andrew



### Problem statement:

#### Programming challenge

A json web service has been set up at the url: http://agl-developer-test.azurewebsites.net/people.json

You need to write some code to consume the json and output a list of all the cats in alphabetical order under a heading of the gender of their owner.

You can write it in any language you like. You can use any libraries/frameworks/SDKs you choose.

Submissions which include tests will be looked upon more favourably

	Example:

	Male

	Angel
	Molly
	Tigger

	Female

	Gizmo
	Jasper

#### Notes

- Use of language features and/or libraries for grouping/sorting is encouraged. Eg: C# -> LINQ, Java8 -> Stream API, Javascript -> Lodash, Python -> List comprehension
- Use of libraries for consumption of web services is encouraged. Eg: C# -> HttpClient, Java -> HttpClient, Javascript -> jQuery
- You can write it in your favourite Text Editor/IDE
- Submissions will only be accepted via github or bitbucket