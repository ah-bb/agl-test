﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AGL.PetOwners.Console.Filters;
using AGL.PetOwners.Console.Gateways;
using AGL.PetOwners.Console.Models;
using AGL.PetOwners.Console.Services;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace AGL.PetOwners.UnitTests.Services
{
    [TestFixture]
    public class PetOwnershipAnalysisServiceTests
    {
        private PetOwnershipAnalysisService CreatePetOwnershipAnalysisService(Mock<IPetOwnerDataStore> petOwnerDataStoreMock = null,
                                                                              Mock<IPetAggregator> petAggregatorMock = null)
        {
            petOwnerDataStoreMock = petOwnerDataStoreMock ?? new Mock<IPetOwnerDataStore>();
            petAggregatorMock = petAggregatorMock ?? new Mock<IPetAggregator>();

            return new PetOwnershipAnalysisService(petOwnerDataStoreMock.Object, petAggregatorMock.Object);
        }

        [Test]
        public async Task RunAnalysisAsync_WithNoPetOwners_ReturnsEmptyList()
        {
            //Arrange
            var petOwnerDataStoreMock = new Mock<IPetOwnerDataStore>();
            petOwnerDataStoreMock.Setup(m => m.FetchPetOwnersAsync())
                                 .ReturnsAsync(new List<PetOwner>());

            var petAggregatorMock = new Mock<IPetAggregator>();
            petAggregatorMock.Setup(m => m.Aggregate(It.IsAny<IList<PetOwner>>()))
                             .Returns(new Dictionary<string, IList<Pet>>());

            var sut = CreatePetOwnershipAnalysisService(petOwnerDataStoreMock, petAggregatorMock);
            
            //Act
            var results = await sut.RunAnalysisAsync();
            
            //Assert
            CollectionAssert.IsEmpty(results);
        }

        [Test]
        public async Task RunAnalysisAsync_WithPetOwners_ReturnsExpectedResults()
        {
            //Arrange
            var expectedResults = new[]
            {
                "Group 1",
                "",
                "* A",
                "* B",
                "",
                "Group 2",
                "",
                "* A",
                "* C"
            };

            var aggregatedData = new Dictionary<string, IList<Pet>>
            {
                {"Group 1", new List<Pet> {new Pet("A", "X"), new Pet("B", "X")}},
                {"Group 2", new List<Pet> {new Pet("A", "X"), new Pet("C", "X")}}
            };

            var petAggregatorMock = new Mock<IPetAggregator>();
            petAggregatorMock.Setup(m => m.Aggregate(It.IsAny<IList<PetOwner>>()))
                             .Returns(aggregatedData);

            var sut = CreatePetOwnershipAnalysisService(petAggregatorMock: petAggregatorMock);

            //Act
            var results = await sut.RunAnalysisAsync();

            //Assert
            results.ShouldAllBeEquivalentTo(expectedResults, options => options.WithStrictOrdering());
        }
    }
}
