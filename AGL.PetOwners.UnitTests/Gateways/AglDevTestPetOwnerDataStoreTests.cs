﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AGL.PetOwners.Console.Gateways;
using AGL.PetOwners.Console.Infrastructure;
using AGL.PetOwners.Console.Models;
using FluentAssertions;
using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using Serilog;

namespace AGL.PetOwners.UnitTests.Gateways
{
    [TestFixture]
    public class AglDevTestPetOwnerDataStoreTests
    {
        private readonly string _embeddedResourceRootFolder = @"AGL.PetOwners.UnitTests.Gateways.TestData";

        private AglDevTestPetOwnerDataStore CreatePetOwnerDataStore(Mock<IDataRetrievalService> dataRetrievalServiceMock = null,
                                                                    Mock<IEndpointConfiguration> endpointConfigMock = null)
        {
            var loggerMock = new Mock<ILogger>();
            dataRetrievalServiceMock = dataRetrievalServiceMock ?? new Mock<IDataRetrievalService>();
            endpointConfigMock = endpointConfigMock ?? new Mock<IEndpointConfiguration>();

            return new AglDevTestPetOwnerDataStore(loggerMock.Object, dataRetrievalServiceMock.Object, endpointConfigMock.Object);
        }

        [Test]
        public async Task FetchPetOwnersAsync_WithOriginalAGLSampleData_DeserializesIntoExpectedDataStructure()
        {
            //arrange
            var expectedResults = DeserializeEmbeddedResource<List<PetOwner>>("OriginalAGLSample.people.json")
                                    .Where(p => p.Pets?.Count > 0);

            var dataRetrievalServiceMock = new Mock<IDataRetrievalService>();
            dataRetrievalServiceMock.Setup(m => m.FetchContentAsync(It.IsAny<Uri>()))
                                    .ReturnsAsync(GetEmbeddedResourceAsString("OriginalAGLSample.people.json"));

            var sut = CreatePetOwnerDataStore(dataRetrievalServiceMock);

            //act
            var results = await sut.FetchPetOwnersAsync();
            
            //assert
            results.ShouldAllBeEquivalentTo(expectedResults);
        }

        [Test]
        public async Task FetchPetOwnersAsync_WhenSourceDataContainsNoPetOwners_ReturnsEmptyList()
        {
            //arrange
            var people = new List<PetOwner>
            {
                new PetOwner("Max", Gender.Male, 2, null),
                new PetOwner("Max", Gender.Male, 2, new List<Pet>())
            };
            var peopleJson = JsonConvert.SerializeObject(people);

            var dataRetrievalServiceMock = new Mock<IDataRetrievalService>();
            dataRetrievalServiceMock.Setup(m => m.FetchContentAsync(It.IsAny<Uri>()))
                                    .ReturnsAsync(peopleJson);

            var sut = CreatePetOwnerDataStore(dataRetrievalServiceMock);

            //act
            var results = await sut.FetchPetOwnersAsync();

            //assert
            CollectionAssert.IsEmpty(results);
        }

        [Test]
        public async Task FetchPetOwnersAsync_WithOnePetOwnerInSourceData_ReturnsPetOwner()
        {
            //arrange
            var expectedPetOwner = new PetOwner("Jill", Gender.Female, 8, new List<Pet> { new Pet("Rex", "Dog")});
            var people = new List<PetOwner>
            {
                new PetOwner("Max", Gender.Male, 2, null),
                new PetOwner("Max", Gender.Male, 2, new List<Pet>()),
                expectedPetOwner
            };
            var peopleJson = JsonConvert.SerializeObject(people);

            var dataRetrievalServiceMock = new Mock<IDataRetrievalService>();
            dataRetrievalServiceMock.Setup(m => m.FetchContentAsync(It.IsAny<Uri>()))
                                    .ReturnsAsync(peopleJson);

            var sut = CreatePetOwnerDataStore(dataRetrievalServiceMock);

            //act
            var results = await sut.FetchPetOwnersAsync();

            //assert
            CollectionAssert.IsNotEmpty(results);
            Assert.AreEqual(1, results.Count);

            var result = results.Single();
            result.ShouldBeEquivalentTo(expectedPetOwner);
        }

        [TestCase("", Description = "Empty")]
        [TestCase(null, Description = "null")]
        [TestCase("{ this is not valid json }", Description = "Invalid json")]
        [TestCase("{ \"data\": \"Valid json but not of type PetOwner\" }", Description = "Valid json but not of type PetOwner")]
        public void FetchPetOwnersAsync_WithInvalidContent_Throws(string content)
        {
            //arrange
            var dataRetrievalServiceMock = new Mock<IDataRetrievalService>();
            dataRetrievalServiceMock.Setup(m => m.FetchContentAsync(It.IsAny<Uri>()))
                                    .ReturnsAsync(content);

            var sut = CreatePetOwnerDataStore(dataRetrievalServiceMock);

            //act and assert
            var ex = Assert.ThrowsAsync<ApplicationException>(async () => await sut.FetchPetOwnersAsync());
            Assert.IsNotNull(ex.Message);
            Assert.AreEqual("Unexpected PetOwner data in json results retrieved from the DataRetrievalService", ex.Message);
        }

        private T DeserializeEmbeddedResource<T>(string resourceName)
        {
            var json = GetEmbeddedResourceAsString(resourceName);
            var actual = JsonConvert.DeserializeObject<T>(json);
            return actual;
        }

        private string GetEmbeddedResourceAsString(string resourceName)
        {
            string val;
            var resourcePath = $"{_embeddedResourceRootFolder.Replace("\\", ".")}.{resourceName.Replace("\\", ".")}";

            var assembly = Assembly.GetExecutingAssembly();
            using (var stream = assembly.GetManifestResourceStream(resourcePath))
            {
                Assert.IsNotNull(stream, $"GetManifestResourceStream returned a null stream for: {resourcePath}");
                using (var reader = new StreamReader(stream))
                {
                    val = reader.ReadToEnd();
                }
            }
            return val;
        }
    }
}