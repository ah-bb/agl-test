﻿using System.Collections.Generic;
using AGL.PetOwners.Console.Filters;
using AGL.PetOwners.Console.Models;
using FluentAssertions;
using NUnit.Framework;

namespace AGL.PetOwners.UnitTests.Filters
{
    [TestFixture]
    public class PetsByOwnerGenderAggregatorTests
    {
        [Test]
        public void Aggregate_WhenPetOwnersListIsEmpty_ReturnsEmptyResult()
        {
            //arrange
            var owners = new List<PetOwner>();
            var sut = new CatsByOwnerGenderAggregator();

            //act
            var res = sut.Aggregate(owners);

            //assert
            CollectionAssert.IsEmpty(res);
        }

        [Test]
        public void Aggregate_WhenPetOwnersListIsNull_ReturnsEmptyResult()
        {
            //arrange
            var sut = new CatsByOwnerGenderAggregator();

            //act
            var results = sut.Aggregate(null);

            //assert
            CollectionAssert.IsEmpty(results);
        }

        [Test]
        public void Aggregate_WithOnlyNonCatPetOwners_ReturnsEmptyResult()
        {
            //arrange
            var owners = new List<PetOwner>
            {
                new PetOwner("Bill", Gender.Male, 11, new List<Pet> { new Pet("Rex", "Dog")}),
                new PetOwner("Bob", Gender.Male, 9, new List<Pet> { new Pet("Blotto", "Fish")})
            };
            var sut = new CatsByOwnerGenderAggregator();

            //act
            var results = sut.Aggregate(owners);

            //assert
            CollectionAssert.IsEmpty(results);
        }

        [Test]
        public void Aggregate_WithTwoCatOwnersOfSameGender_GroupsTogether()
        {
            //arrange
            var owners = new List<PetOwner>
            {
                new PetOwner("Bill", Gender.Male, 11, new List<Pet> { new Pet("Felix", "Cat")}),
                new PetOwner("Bob", Gender.Male, 11, new List<Pet> { new Pet("Waldo", "Cat")})
            };
            var sut = new CatsByOwnerGenderAggregator();

            //act
            var results = sut.Aggregate(owners);

            //assert
            Assert.AreEqual(1, results.Keys.Count);
            Assert.IsTrue(results.ContainsKey(Gender.Male.ToString()));
            Assert.AreEqual(2, results[Gender.Male.ToString()].Count);
            Assert.IsFalse(results.ContainsKey(Gender.Female.ToString()));
        }

        [Test]
        public void Aggregate_WithTwoCatOwnersOfDifferentGender_DoesNotGroupTogether()
        {
            //arrange
            var owners = new List<PetOwner>
            {
                new PetOwner("Bill", Gender.Male, 11, new List<Pet> { new Pet("Felix", "Cat")}),
                new PetOwner("Mary", Gender.Female, 11, new List<Pet> { new Pet("Waldo", "Cat")})
            };
            var sut = new CatsByOwnerGenderAggregator();

            //act
            var results = sut.Aggregate(owners);

            //assert
            Assert.IsTrue(results.ContainsKey(Gender.Male.ToString()));
            Assert.AreEqual(1, results[Gender.Male.ToString()].Count);
            Assert.IsTrue(results.ContainsKey(Gender.Female.ToString()));
            Assert.AreEqual(1, results[Gender.Female.ToString()].Count);
        }

        [Test]
        public void Aggregate_WithMixtureOfPetsAndGenders_MatchesExpectedGroupingWithPetNamesAlphabeticallySorted()
        {
            //arrange
            var owners = new List<PetOwner>
            {
                new PetOwner("Bill", Gender.Male, 11, new List<Pet> { new Pet("Felix", "Cat") }),
                new PetOwner("Bob", Gender.Male, 12, new List<Pet> { new Pet("Waldo", "Cat"), new Pet("Alpha", "Dog") }),
                new PetOwner("Mary", Gender.Female, 13, new List<Pet> { new Pet("Pinny", "Cat") }),
                new PetOwner("Tina", Gender.Female, 11, new List<Pet> { new Pet("Bose", "Cat") }),
                new PetOwner("Jenny", Gender.Female, 4, null),
                new PetOwner("Peter", Gender.Male, 2, new List<Pet>()),
                new PetOwner("Simon", Gender.Male, 12, new List<Pet> { new Pet("Gulp", "Fish")})
            };

            var expectedResults = new Dictionary<string, IList<Pet>>
            {
                {Gender.Male.ToString(), new List<Pet> { new Pet("Felix", "Cat"), new Pet("Waldo", "Cat") }},
                {Gender.Female.ToString(), new List<Pet> { new Pet("Bose", "Cat"), new Pet("Pinny", "Cat") }},
            };

            var sut = new CatsByOwnerGenderAggregator();

            //act
            var results = sut.Aggregate(owners);

            //assert
            results.ShouldAllBeEquivalentTo(expectedResults, options => options.WithStrictOrdering());
        }
    }
}